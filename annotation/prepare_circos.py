#!/usr/bin/env python

import sys

data = sys.argv[1]
gene, trna = set(), set()


trnas = open(data+"/circos_trna.txt", "wt")
genes = open(data+"/circos_genes.txt", "wt")

with open(data+"/webapollo_annotations.gff") as f:
    for line in f:
        if line.startswith("##FASTA"):
            break
        if line.startswith("#"):
            continue
        l = line.split()
        if "trnascan" in line:
            trna.add(("mth", l[4], l[5]))
        elif l[3] == "gene":
            gene.add(("mth", l[4], l[5]))


trnas.writelines("\n".join(["\t".join(t) for t in sorted(trna, key=lambda x: int(x[1]))]))
genes.writelines("\n".join(["\t".join(g) for g in sorted(gene, key=lambda x: int(x[1]))]))

trnas.close()
genes.close()
