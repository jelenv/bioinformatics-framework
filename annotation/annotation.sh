#!/usr/bin/env bash

source ../__imports.sh

### Paths to data
DATA_FOLDER=$ANNOTATION_DATA
GBPROTEINS=$DATA_FOLDER/verticillium_mito_proteins.fasta #downloaded from genbank with the query:  (fungi[Organism]) AND mitochondrial AND verticillium 
TRANSCRIPTS=$DATA_FOLDER/transcripts.gtf
GENOME=${DATA_FOLDER}/tab2_mitohondrij.fasta

##############################################################
# PART 1: Cufflinks transcript improvement with Transdecoder #
##############################################################

### A hardcoded example I ran on a remote server to get the  transcripts.gtf file
#tophat -o mapiranje --library-type fr-unstranded -p 8 mito rnaseq/pg1_1_1.fastq.gz,rnaseq/pg1_1_2.fastq.gz,rnaseq/pg1_2_1.fastq.gz,rnaseq/pg1_2_2.fastq.gz,rnaseq/pg1_3_1.fastq.gz,rnaseq/pg1_3_2.fastq.gz rnaseq/pg2_1_1.fastq.gz,rnaseq/pg2_1_2.fastq.gz,rnaseq/pg2_2_1.fastq.gz,rnaseq/pg2_2_2.fastq.gz,rnaseq/pg2_3_1.fastq.gz,rnaseq/pg2_3_2.fastq.gz

### Run cufflinks to assemble the transcripts
#cufflinks -o transkripti -p 8 -u -b tab2_mitohondrij.fasta mapiranje/accepted_hits.bam

### Improve the transcripts with Transcoder
if [ ! -e ${DATA_FOLDER}/transdecoder_transcripts.gff3 ]; then
	${TRANSDECODER}/util/cufflinks_gtf_to_alignment_gff3.pl ${TRANSCRIPTS} > ${DATA_FOLDER}/cuff_transcripts.gff3
	${TRANSDECODER}/util/cufflinks_gtf_genome_to_cdna_fasta.pl ${DATA_FOLDER}/transcripts.gtf ${GENOME} > ${DATA_FOLDER}/cuff_transcripts.fasta
	${TRANSDECODER}/TransDecoder -t ${DATA_FOLDER}/cuff_transcripts.fasta --CPU `nproc` #--search_pfam /home/vid/TransDecoder_r20140704/pfam/Pfam-AB.hmm.bin 
	${TRANSDECODER}/cdna_alignment_orf_to_genome_orf.pl ${DATA_FOLDER}/transcripts.fasta.transdecoder.gff3 ${DATA_FOLDER}/cuff_transcripts.gff3 ${DATA_FOLDER}/cuff_transcripts.fasta
	rm -fr transdecoder.* *.dat *.pep *.mRNA *.cds *.bed
	mv cuff_transcripts.* ${DATA_FOLDER}/transdecoder_transcripts.gff3
else
	log DONE "Part 1 (Transcript improvement with Transdecoder) complete!"
fi

########################################
# PART 2: Prepare input data for Maker #
########################################

# Do an ab-initio prediction with GeneMarkS
if [ ! -e ${DATA_FOLDER}/final_gene_annotations.gff ]; then
	${GMSN} --prok --gcode 4 --shape circular --format GFF --output ${DATA_FOLDER}/genemark_genes.gff3 ${GENOME} #mitochondrial specific gene prediction
	rm -f gms.log GeneMark_heuristic.mat gm_4.tbl
	mv GeneMark_hmm_heuristic.mod ${DATA_FOLDER}/

	back=`pwd`
	cd ${TRNASCAN}
	${MAKER} -CTL
	#Set the paths for maker
	sed -i "s|gmhmmp=|gmhmmp=/home/vid/genemarkS/gmsuite/gmhmmp|g; s|probuild=|probuild=/home/vid/genemarkS/gmsuite/probuild|g" maker_exe.ctl
	sed -i "s|genome= #|genome=${back}/${GENOME} #|g; s|=eukaryotic|=prokaryotic|g; s|protein_pass=0|protein_pass=1|g; s|model_pass=0|model_pass=1|g; s|pred_pass=0|pred_pass=1|g" maker_opts.ctl
	sed -i "s|protein=  #|protein=${back}/${GBPROTEINS}  #|g; s|gmhmm=|gmhmm=${back}/${DATA_FOLDER}/GeneMark_hmm_heuristic.mod|g; s|protein2genome=0|protein2genome=1|g; s|cpus=1|cpus=`${NPROC}`|g" maker_opts.ctl
	sed -i "s|clean_try=0|clean_try=1|g; s|clean_up=0|clean_up=1|g" maker_opts.ctl
	sed -i "s|single_exon=0|single_exon=1|g" maker_opts.ctl
	sed -i "s|est_pass=0 #use ESTs|est_pass=1 #use ESTs|g" maker_opts.ctl
	sed -i "s|est_gff= #aligned ESTs or|est_gff=${back}/${DATA_FOLDER}/transdecoder_transcripts.gff3 #aligned ESTs or|g" maker_opts.ctl
	sed -i "s|est= #set of|est=${back}/${DATA_FOLDER}/cuff_transcripts.fasta #set of|g" maker_opts.ctl
	sed -i "s|trna=0|trna=1|g" maker_opts.ctl


#####################
# PART 3: Run Maker #
#####################

	${MAKER} -RM_off -nodatastore
	rm *.ctl
	contigname=`head ${back}/${GENOME} -n 1`
	mv `basename ${GENOME%.*}`.maker.output/`basename ${GENOME%.*}`_datastore/${contigname:1:-1}/${contigname:1:-1}.gff ${back}/${DATA_FOLDER}/final_gene_annotations.gff
	rm -r `basename ${GENOME%.*}`*
	cd ${back}
else
	log DONE "Part 2 & 3 (Prepare input data and run Maker) complete!"
fi	


####################################################
# PART 4: Prepare gene and t-RNA tracks for Circos #
####################################################

if [ ! -e ${DATA_FOLDER}/webapollo_annotations.gff ]; then
	log ERROR "Perform manual annotation of maker genes in webapollo and place the resulting file in ${DATA_FOLDER}, with the name webapollo_annotations.gff"
else
	if [ ! -e ${DATA_FOLDER}/circos_genes.txt ] || [ ! -e ${DATA_FOLDER}/circos_trna.txt ]; then
		python prepare_circos.py ${DATA_FOLDER}
		log INFO "Input Gene and t-RNA tracks for Circos prepared in ${DATA_FOLDER}"
	else
		log DONE "Part 4 (Prepare gene and t-RNA tracks for Circos) complete
