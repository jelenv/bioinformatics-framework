# Annotation
-----------


This pipeline uses an input genome _(.fasta)_ and other input files to produce a final genome annotation based on:


- _ab-initio_ annotations
- RNA-Seq guided annotations
- homology guided annotations (ESTs, proteins)

