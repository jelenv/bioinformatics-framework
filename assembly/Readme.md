# Assembly
----------

The pipeline uses input reads _(.fastq)_ or _(.fastq.gz)_ with an external pipeline for _de-novo_ assembly, to produce a best effort scaffolded genome.

**Programs**


- A5	
- Velvet Optimiser
