#!/usr/bin/env python
# encoding: utf-8

import gzip
import argparse
from optparse import OptionParser

usage = "usage: interleave_fastq.py [options] <fastq_1, fast_q2> <out_file>"
option_parser = OptionParser(usage=usage)
option_parser.add_option('-1', '--first-input', dest="first_input", metavar="<in_file1>",
        help="The first input file in .fastq or .fastq.gz format.")
option_parser.add_option('-2', '--second-input', dest="second_input" , metavar="<in_file2>",
        help="The second input file in .fastq or .fastq.gz format.")
option_parser.add_option('-o', '--output', dest="output", metavar="<out_file>",
        help="The output file in fastq format.")
options, args = option_parser.parse_args()

def process_reads(args):
    if args.first_input.endswith(".gz") or args.second_input.endswith('.gz'):
        first = gzip.open(args.first_input, 'rb')
        second = gzip.open(args.second_input, 'rb')
        output = gzip.open(args.output, 'wb')
    else:
        first = open(args.first_input, 'rU')
        second = open(args.second_input, 'rU')
        output = open(args.output, 'wb')

    while True:

        # Read and write to output 4 lines at a time (1 fastq read)
        first_fastq = first.readline()
        if not first_fastq:
            break
        output.write(first_fastq)

        first_fastq = first.readline()
        output.write(first_fastq)

        first_fastq = first.readline()
        output.write(first_fastq)

        first_fastq = first.readline()
        output.write(first_fastq)


        # Same thing with the second fastq file
        second_fastq = second.readline()
        output.write(second_fastq)

        second_fastq = second.readline()
        output.write(second_fastq)

        second_fastq = second.readline()
        output.write(second_fastq)

        second_fastq = second.readline()
        output.write(second_fastq)

    first.close()
    second.close()
    output.close()

    return

if __name__ == '__main__':
    try:
        process_reads(options)
    except:
        option_parser.print_help()
        exit()

