#!/usr/bin/env bash

source ../__imports.sh

### Paths to data
DATA_FOLDER=$ASSEMBLY_DATA 
SAMPLE="1953"
PROGRAM=$A5

##############################################################
# PART 1: Prepare interleaved reads for the assembly process # 
##############################################################

if [ ! -e ${DATA_FOLDER}/${SAMPLE}/interleaved_reads.fastq ]; then 
	log INFO "Preparing interleaved reads for the assembly step of the pipeline..."
	python interleave_fastq.py -1 ${DATA_FOLDER}/${SAMPLE}/*1.fastq.gz -2 ${DATA_FOLDER}/${SAMPLE}/*2.fastq.gz -o ${DATA_FOLDER}/${SAMPLE}/interleaved_reads.fastq
	log INFO "Interleaved reads successfully prepared!"
else
	log DONE "Interleaved reads already prepared!"
fi


#############################################################################
# PART 2: Use interleaved reads from previous step in the assembly pipeline # 
#############################################################################


if [ ! -d ${DATA_FOLDER}/${SAMPLE}/assembly_results ]; then
	if [ $PROGRAM == $A5 ]; then
		log INFO "Starting assembly with the A5 pipeline"
		perl ${A5} --threads=${NPROC} ${DATA_FOLDER}/${SAMPLE}/*1.fastq.gz ${DATA_FOLDER}/${SAMPLE}/*2.fastq.gz ${DATA_FOLDER}/${SAMPLE}/assembly_results
	else
		log INFO "Starting assembly with the Velvet Optimiser pipeline"
		perl ${VELOPT} -t ${NPROC} -p results -d ${DATA_FOLDER}/${SAMPLE}/assembly_results -f '-shortPaired -fastq ${DATA_FOLDER}/${SAMPLE}/interleaved_reads.fastq'
	fi
	log INFO "Pipeline finished. Lookup your results in the ${DATA_FOLDER}/${SAMPLE}/assembly_results folder."
else
	log DONE "Assembly already complete!"
fi


#perl /home/vid/VelvetOptimiser-2.2.5/VelvetOptimiser.pl -f '-shortPaired -fastq interleaved_reads.fastq' -t 4
