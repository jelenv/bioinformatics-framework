# Phylogeny
-----------

The pipeline uses input variant annotations _(.vcf)_, to concatenate the variant calls into a _.fasta_ file, which can then be used for alignment and phylogeny in MEGA.

