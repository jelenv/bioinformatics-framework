#!/usr/bin/env bash

source ../__imports.sh

### Paths to data
DATA_FOLDER=$PHYLOGENY_DATA
SNPFILTERS="-d 10 -D 1000000 -Q 20"
GQ_FILTER=0

#############################################################################
# PART 1: Prepare filtered SNP calls from a reference and BAM mapping files #
#############################################################################

ls ${DATA_FOLDER}/filtered_calls.vcf 2>&1 > /dev/null
if [ ! $? -eq 0 ]; then
    log INFO "Pileup and raw SNP calling finished... Commencing final filtering of SNP calls."
    ${BCFTOOLS} view ${DATA_FOLDER}/raw_calls.bcf | ${VCFUTILS} varFilter ${SNPFILTERS} > ${DATA_FOLDER}/filtered_calls.vcf
	cat ${DATA_FOLDER}/filtered_calls.vcf | grep -v "0/1:" > ${DATA_FOLDER}/filtered_calls_homozygous.vcf ##Take only the SNPS that are declared homozygous in all of the samples
    log INFO "Job complete.. SNP calls prepared for further downstream analysis"
fi

if [ ! -f ${DATA_FOLDER}/annotated_snps.vcf ]; then
   log INFO "Annotating filtered SNP calls with snpEff..." 
   java -Xmx4g -jar ${SNPEFF}/snpEff.jar eff -c ${SNPEFF}/snpEff.config -stats ${DATA_FOLDER}/snpEff_summary.html -v vaa ${DATA_FOLDER}/filtered_calls_homozygous.vcf > ${DATA_FOLDER}/annotated_snps.vcf
fi

if [ ! -f ${DATA_FOLDER}/phylogenies.fasta ]; then
	python msrefs.py ${DATA_FOLDER}/annotated_snps.vcf ${GQ_FILTER} > ${DATA_FOLDER}/phylogenies.fasta
	cat ${DATA_FOLDER}/annotated_snps.vcf | grep -v "chr-unplaced" > ${DATA_FOLDER}/nounplaced_snps.vcf
	python msrefs.py ${DATA_FOLDER}/nounplaced_snps.vcf ${GQ_FILTER} > ${DATA_FOLDER}/phylogenies_nounplaced.fasta
fi

if [ ! -f ${DATA_FOLDER}/tree.nex ]; then
	fasttree -pseudo -nt ${DATA_FOLDER}/phylogenies.fasta > ${DATA_FOLDER}/tree.nex
	fasttree -pseudo -nt ${DATA_FOLDER}/phylogenies_nounplaced.fasta > ${DATA_FOLDER}/tree_nounplaced.nex
fi
