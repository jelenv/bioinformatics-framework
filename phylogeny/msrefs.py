from collections import defaultdict
import sys

infile = sys.argv[1]
fastas = defaultdict(str)
positions = {}
gq_cutoff = int(sys.argv[2])

##Multi-sample references

with open(sys.argv[1]) as f:
	for line in f:
		if line.startswith("##"):
			continue
		if line.startswith("#CHROM"):
			ref_pos = line.split().index("REF")
			alt_pos = line.split().index("ALT")
			samples_pos = line.split().index("FORMAT") + 1
			for k in line.split()[samples_pos:]:
				fastas[k] = ""
				positions[str(line.split().index(k))] = k 
			continue
		max_len = len(max(line.split()[ref_pos], line.split()[alt_pos].split(",")[-1], key=len)) #get the longest insert so the others can be padded to this length
		offset = -1

		for l in line.split()[samples_pos:]:
			offset += 1
			gt = l.split(":")[0]
			probs = set(l.split(":")[1].split(","))
			gq = int(l.split(":")[2])
			alt = line.split()[alt_pos].split(",")[-1]
			ref = line.split()[ref_pos]
			if gt == "1/1" and len(probs) > 1 and gq > gq_cutoff:
				fastas[positions[str(offset+samples_pos)]] += alt + (max_len-len(alt))*"-"
			elif gt == "0/0" and len(probs) > 1 and gq > gq_cutoff:
				fastas[positions[str(offset+samples_pos)]] += ref + (max_len-len(ref))*"-"
			elif len(probs) == 1 or gq <= gq_cutoff: #very dubious variant call (all genotypes equally probable) or the genotype quality is very low (below gq_cutoff)
				fastas[positions[str(offset+samples_pos)]] += (max_len)*"-"

for k in fastas:
	print ">"+k
	print fastas[k]

