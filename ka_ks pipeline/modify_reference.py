import sys

VCF_FILE	= sys.argv[1]
REFERENCE	= sys.argv[2]
GQ_CUTOFF 	= int(sys.argv[3])

entry, seq = "", ""
gq_cutoff = GQ_CUTOFF

def vcf_modifications(vcffile):
    vcf_mods = {}
    with open(vcffile, "rt") as f:
        for line in f:
            if line.startswith("##"):
                continue
            if line.startswith("#CHROM"):
                ref_pos = line.split().index("REF")
                alt_pos = line.split().index("ALT")
                samples_pos = line.split().index("FORMAT") + 1
                samples = line.split()[samples_pos:]
                continue
            else:
                ln = line.split()
                index = -1
                for l in ln[samples_pos:]:
                    index += 1
                    gt = l.split(":")[0]
                    probs = set(l.split(":")[1].split(","))
                    gq = int(l.split(":")[2])
                    alt = line.split()[alt_pos].split(",")[-1]
                    ref = line.split()[ref_pos]
                    if gt == "1/1" and len(probs) > 1 and gq > gq_cutoff:
                        vcf_mods[ln[0], int(ln[1]), samples[index]] = ln[3], ln[4]
                    elif gt == "0/0" and len(probs) > 1 and gq > gq_cutoff:
                        vcf_mods[ln[0], int(ln[1]), samples[index]] = ln[3], ln[4]
                    else: #very dubious variant call (all genotypes equally probable) or the genotype quality is very low (below gq_cutoff)
                        continue
	return vcf_mods, samples

mods, samples = vcf_modifications(VCF_FILE)

def modify(seq, mods, g, sample):
    for i in sorted(mods, reverse=True, key=lambda x: x[1]):
        if g[1:] == i[0] and sample in i:
            seq = seq[:i[1]-1] + mods[i][1] + seq[i[1]-1+len(mods[i][0]):]
    return seq

with open(REFERENCE, "rt") as f:
    chromosomes = {}
    for l in f:
        if l.startswith(">") and not entry:
            entry = l.rstrip()
            continue
        elif l.startswith(">") and entry:
            chromosomes[entry] = seq
            entry = l.rstrip()
            seq = ""
            continue
        seq += l.rstrip()
    chromosomes[entry] = seq

for sample in samples:
    with open(sample[:sample.index("sorted")]+"modified.fasta", "wt") as outf:
        for c in chromosomes:
            mod_seq = modify(chromosomes[c], mods, c, sample)
            outf.write(c+"\n"+mod_seq+"\n")
