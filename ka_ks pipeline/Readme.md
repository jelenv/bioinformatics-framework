# Ka/Ks analysis
----------------

The pipeline uses an input genome _(.fasta)_, mapping _(.bam)_, gene annotations _(.gff)_,
optional repeat annotations _(.gff)_ and produces a lot of results files. 
Here are the output files documented as per what they include.

| **File(s)**						| **Explanation**																											|
|:---------------------------------:|:--------------------------------------------------------------------------------------------------------------------------|
|*.bcf*								| The raw pileups from bam files needed for further processing into SNP calls												|
|*.vcf*								| The processed and filtered variants found in the mapping files. These are used as input to snpEff for further annotation	|
|*annotated.vcf*					| The _snpEff_ annotated files. These are considered the **final** variant-call results files								|
|*annotated_inrepeats*				| The variants that are present in the locations of the repeat annotations file												|
|*snpEff*							| An HTML report of the snpEff annotation and a _.txt_ report of the genes													|
|*modified*							| Modified versions of the original genome/genes per _.bam_ sample to use for the Ka/Ks calculation							|
|*results*							| Final Ka/Ks results of the pipeline																						|
|*snp_statistics.txt*				| Statistics of variants in the annotated _.vcf_ files																		|
|*zigosity_report.txt* (optional)	| Reports of number of homozygous calls if *hetero_filter* is set to true													|
