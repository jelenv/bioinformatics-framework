#!/usr/bin/env python

from collections import defaultdict
from pylab import *
from random import random
import sys

wins = defaultdict(int)
infile = sys.argv[1]
analysis = sys.argv[2]
folder = sys.argv[3]
win_size = int(sys.argv[4])

with open(infile) as f:
    for line in f:
        l = line.split()
        wins[l[0], int(l[1]), int(l[2])] += int(l[-1])

def draw_subplot(axarr, x, y, coord,max_y, title, color):
    axarr[coord].set_title(title)
    axarr[coord].set_ylabel("%% %s density" % ("exon" if analysis == "exon" else "repeat"))
    axarr[coord].set_ylim(0, int(max_y))
    axarr[coord].plot(x, y, color=color)
    return axarr

for k, v in wins.items():
    wins[k] = v/(win_size * 1.0)

chroms = set([i[0] for i in wins.keys()])
max_x = max([sorted([i for i in wins.keys() if i[0] in c])[-1] for c in chroms], key=lambda x: x[2])[-1]
f, axarr = subplots(1,1)

for i, c in enumerate(chroms):
    f, axarr = subplots(1,1)
    s = sorted([i for i in wins.keys() if i[0] in c])
    x = range(0, s[-1][-1]+1, win_size)
    y = [wins[g] for g in s] 
    f.suptitle("%s density for %s" % ("Exon" if analysis == "exon" else "Repeat" , c), fontsize=14, fontweight="bold")
    axarr.set_ylabel("%% of %s density" % ("exon" if analysis == "exon" else "repeat"))
    axarr.set_ylim(0, 1)
    axarr.set_xlim(0, max_x)
    axarr.plot(x,y, color="black")

    f.set_size_inches(24,12)
    savefig("%s/%s_density_%s.png" % (folder, "exon" if analysis == "exon" else "repeat", c), dpi=100)
