#!/usr/bin/env bash

source ../__imports.sh

### Paths to data
DATA_FOLDER=${KAKS_DATA}
REFERENCE=${DATA_FOLDER}/vert2.fasta
GENE_MODELS=${DATA_FOLDER}/evm_all.gff3
REPEATS=${DATA_FOLDER}/repeats_de-novo.gff3
VARSUBSETS=${DATA_FOLDER}/variant_subsets
EXONDENSITY=${DATA_FOLDER}/exon_density
REPEATDENSITY=${DATA_FOLDER}/repeat_density
SNPDENSITY=${DATA_FOLDER}/snp_density

### Filter settings for SNP calls
SNPFILTERS="-d 10 -D 1000000 -Q 20"
GQ_CUTOFF=0

### Window size for the sliding-window snp distribution analysis
WINDOWSIZE=10000

#############################################################################
# PART 1: Prepare filtered SNP calls from a reference and BAM mapping files #
#############################################################################

if [ ! `ls ${DATA_FOLDER}/*.bai 2> /dev/null | wc -l ` -eq `ls ${DATA_FOLDER}/*mapping.bam 2> /dev/null | wc -l ` ]; then
	log WARN "Mapping files do not seem to be sorted and indexed yet. Starting samtools... This might take a while"
	for f in `ls ${DATA_FOLDER}/*.bam`; do
		log INFO "Sorting and indexing file ${f}"
		${SAMTOOLS} sort -@ ${NPROC} -m 1G ${f} ${f%_*}_sorted
		${SAMTOOLS} index ${f%_*}_sorted.bam
	done
	log INFO "Done"
fi

ls ${DATA_FOLDER}/raw_calls.bcf 2>&1 > /dev/null
if [ ! $? -eq 0 ]; then
    log INFO "BAM Files sorted and indexed... Preparing pileup and unfiltered SNP calls of input files"
    ${SAMTOOLS} mpileup -E -uf ${REFERENCE} ${DATA_FOLDER}/*sorted.bam | ${BCFTOOLS} view -bvcgNe - > ${DATA_FOLDER}/raw_calls.bcf
    log INFO "Raw SNP calls prepared."
fi

ls ${DATA_FOLDER}/filtered_calls.vcf 2>&1 > /dev/null
if [ ! $? -eq 0 ]; then
    log INFO "Pileup and raw SNP calling finished... Commencing final filtering of SNP calls."
    ${BCFTOOLS} view ${DATA_FOLDER}/raw_calls.bcf | ${VCFUTILS} varFilter ${SNPFILTERS} > ${DATA_FOLDER}/filtered_calls.vcf
    cat ${DATA_FOLDER}/filtered_calls.vcf | grep -v "0/1:" > ${DATA_FOLDER}/filtered_calls_homozygous.vcf
	echo "scale=4 ; `cat ${DATA_FOLDER}/filtered_calls_homozygous.vcf | grep -v '#' | wc -l`/`cat ${DATA_FOLDER}/filtered_calls.vcf | grep -v '#' | wc -l`" | bc > ${DATA_FOLDER}/zigosity_report.txt
	echo "Percent of variants declared homozygous in the provided samples" >> ${DATA_FOLDER}/zigosity_report.txt
    log INFO "Job complete.. SNP calls prepared for further downstream analysis"
fi

### BACKGROUND ON FILTERING
#Heterozygous calls in homozygous organisms usually originate from repetitive elements, that have multiple copies in the genome. 
#Some reads from repeat_1 map to repeat_2 and vice/versa, thus making it seem a heterozygous call. 

if [ ! -f ${DATA_FOLDER}/annotated_snps.vcf ]; then
   log INFO "Annotating filtered SNP calls with snpEff..."
   java -Xmx4g -jar ${SNPEFF}/snpEff.jar eff -c ${SNPEFF}/snpEff.config -stats ${DATA_FOLDER}/snpEff_summary.html -v vaa ${DATA_FOLDER}/filtered_calls_homozygous.vcf > ${DATA_FOLDER}/annotated_snps.vcf
fi
log DONE "Part 1 of the pipeline (SNP calling and annotation) complete."


####################################################
# PART 2: SNP statistics and repeat mask filtering #
####################################################

if [ -e ${DATA_FOLDER}/snp_statistics.txt ]; then
	log DONE "Part 2 of the pipeline (SNP statistics) complete."
else
	if [ -f ${DATA_FOLDER}/annotated_snps.vcf ]; then
		if [ ! -z ${REPEATS} ]; then
			log INFO "Doing SNP statistics on the snpEff annotated .vcf files"
			${BEDTOOLS} intersect -a ${DATA_FOLDER}/annotated_snps.vcf -b ${REPEATS} > ${DATA_FOLDER}/snps_inrepeats.vcf
			echo "Percent of variants in ${REPEATS} repetitive regions" >> ${DATA_FOLDER}/snp_statistics.txt
			echo "scale=4; `cat ${DATA_FOLDER}/snps_inrepeats.vcf | grep -v '#' | wc -l`/`cat ${DATA_FOLDER}/annotated_snps.vcf | grep -v '#' | wc -l`" | bc >> ${DATA_FOLDER}/snp_statistics.txt
			${VCF_STATS} ${DATA_FOLDER}/annotated_snps.vcf >> ${DATA_FOLDER}/snp_statistics.txt  
		fi
		log INFO "Job complete... See the statistics in snp_statistics.txt"
	fi
fi


##################################################################
# PART 3: Variant subsets analysis and SNP distribution analysis #
##################################################################

if [ `ls ${SNPDENSITY}/*.png 2> /dev/null | wc -l ` -gt 0 ]; then
	log DONE "Part 3 of the pipeline (SNP distribution analysis) complete."
else
	if [ -f ${DATA_FOLDER}/annotated_snps.vcf ]; then
	
		if [ ! -d ${VARSUBSETS} ]; then
			log INFO "Getting subsets of variants for detailed analysis"
			
			mkdir -p ${VARSUBSETS}

			## Get all variants that are in the genes
			${BEDTOOLS} intersect -u -a ${DATA_FOLDER}/annotated_snps.vcf -b ${GENE_MODELS} > ${VARSUBSETS}/vars_in_genes.vcf
			
			## Get number of variants for each gene, where the number is greater than 1
			${BEDTOOLS} intersect -c -b ${DATA_FOLDER}/annotated_snps.vcf -a ${GENE_MODELS} | awk '$NF > 0 {print $0}' > ${VARSUBSETS}/vars_per_gene_over_1.gff

			## Get all genes that have variants 
			${BEDTOOLS} intersect -wa -a ${GENE_MODELS} -b ${DATA_FOLDER}/annotated_snps.vcf > ${VARSUBSETS}/genes_with_variants.gff

			## Get number of variants for each gene 
			${BEDTOOLS} intersect -c -a ${GENE_MODELS} -b ${DATA_FOLDER}/annotated_snps.vcf > ${VARSUBSETS}/vars_per_gene.gff

			## Get indels in the variants
			grep "INDEL" ${DATA_FOLDER}/annotated_snps.vcf | tail -n +2 > ${VARSUBSETS}/indels.vcf

			## Get number of indels in the variants
			wc -l ${VARSUBSETS}/indels.vcf > ${VARSUBSETS}/no_of_indels.txt

			## Get indels that are in genes
			${BEDTOOLS} intersect -u -a ${VARSUBSETS}/indels.vcf -b ${GENE_MODELS} > ${VARSUBSETS}/indels_in_genes.vcf

			## Get number of indels for each gene, where the number is greater than 1
			${BEDTOOLS} intersect -c -b ${VARSUBSETS}/indels.vcf -a ${GENE_MODELS} | awk '$NF > 0 {print $0}' > ${VARSUBSETS}/indels_per_gene_over_1.gff
		fi

		if [ ! -f ${DATA_FOLDER}/reference.windows ]; then
			log INFO "Preparing sliding windows for the SNP distribution analysis"			
			python fasta_size.py ${REFERENCE} > ${DATA_FOLDER}/genome_sizes.txt
			${BEDTOOLS} makewindows -g ${DATA_FOLDER}/genome_sizes.txt -w ${WINDOWSIZE} > ${DATA_FOLDER}/reference.windows
		fi

		if [ ! `ls ${DATA_FOLDER}/*_windows 2> /dev/null | wc -l ` -eq `ls ${DATA_FOLDER}/*sorted.bam 2> /dev/null | wc -l ` ]; then
			log INFO "Retrieving SNP's and drawing a SNP distribution graph"
			ls ${DATA_FOLDER}/*sorted.bam | parallel "python parse_vcf.py ${DATA_FOLDER}/annotated_snps.vcf {} > ${DATA_FOLDER}/{.}_subset.vcf && ${BEDTOOLS} intersect -c -a ${DATA_FOLDER}/reference.windows -b ${DATA_FOLDER}/{.}_subset.vcf > ${DATA_FOLDER}/{.}_windows"
		fi
	
		if [ ! -d ${SNPDENSITY} ]; then
			mkdir -p ${SNPDENSITY}
		fi

		log INFO "Drawing SNP distribution with matplotlib"
		python draw_snps.py ${DATA_FOLDER}/reference.windows ${SNPDENSITY} `ls ${DATA_FOLDER}/*_windows`
		log INFO "Job complete... See individual files of this analysis for more details"
	fi
fi


############################################
# PART 4: Exon and Repeat density analysis #
############################################

if [ `ls ${EXONDENSITY}/*.png 2> /dev/null | wc -l ` -gt 0 ] && [ `ls ${REPEATDENSITY}/*.png 2> /dev/null | wc -l ` -gt 0 ]; then
	log DONE "Part 4 of the pipeline (Exon and Repeat density analysis) complete."
else
	if [ ! -d ${EXONDENSITY} ] && [ ! -d ${REPEATDENSITY} ]; then
		mkdir -p ${EXONDENSITY} ${REPEATDENSITY}
	fi

	log INFO "Conducting the Exon and Repeat density analyses"

	if [ ! -f ${DATA_FOLDER}/reference.windows ]; then
		log INFO "Preparing sliding windows for the density analyses"			
		python fasta_size.py ${REFERENCE} > ${DATA_FOLDER}/genome_sizes.txt
		${BEDTOOLS} makewindows -g ${DATA_FOLDER}/genome_sizes.txt -w ${WINDOWSIZE} > ${DATA_FOLDER}/reference.windows
	fi

	if [ ! `ls ${EXONDENSITY}/*.png 2> /dev/null | wc -l ` -gt 0 ]; then
		## Get exons only out of the gene models
		grep exon ${GENE_MODELS} > ${DATA_FOLDER}/exons.gff3			
		${BEDTOOLS} intersect -wao -a ${DATA_FOLDER}/reference.windows -b ${DATA_FOLDER}/exons.gff3 > ${DATA_FOLDER}/exon_density.tab		
		python density.py ${DATA_FOLDER}/exon_density.tab exon ${EXONDENSITY} ${WINDOWSIZE}
		log INFO "Exon density graphs drawn!"
	fi

	if [ ! `ls ${REPEATDENSITY}/*.png 2> /dev/null | wc -l ` -gt 0 ]; then
		${BEDTOOLS} intersect -wao -a ${DATA_FOLDER}/reference.windows -b ${REPEATS} > ${DATA_FOLDER}/repeat_density.tab		
		python density.py ${DATA_FOLDER}/repeat_density.tab repeat ${REPEATDENSITY} ${WINDOWSIZE}
		log INFO "Repeat density graphs drawn!"
	fi

	log INFO "Density analyses complete! Check the ${EXONDENSITY} and ${REPEATDENSITY} folders for their respective graphs."
fi


#########################################################################################################################################################
# PART 5: Modify the reference coding sequences from the GENE_MODELS file according to the VCF snp-s, extract gene sequences and run the Ka/Ks analysis #
#########################################################################################################################################################

if [ ! -f ${DATA_FOLDER}/annotated_snps.vcf ]; then
	log ERROR "The multi-pileup VCF file in ${DATA_FOLDER} is not present... Rerun the first part of the pipeline again to generate it."
elif [ `ls ${DATA_FOLDER}/results*.kaks 2> /dev/null | wc -l` -eq `ls ${DATA_FOLDER}/*mapping.bam 2> /dev/null | wc -l ` ]; then
	log DONE "Part 5 of the pipeline (Ka/Ks analysis) complete."
else
	if [ ! `ls ${DATA_FOLDER}/*mapping.bam 2> /dev/null | wc -l ` -eq `ls ${DATA_FOLDER}/*modified.fasta 2> /dev/null | wc -l ` ]; then
		log INFO "Preparing modified sequences per their respective SNP calls"
		grep -v "INDEL" ${DATA_FOLDER}/annotated_snps.vcf > ${DATA_FOLDER}/no_indels.vcf #throw out the indels
		python modify_reference.py ${DATA_FOLDER}/no_indels.vcf ${REFERENCE} ${GQ_CUTOFF}
	fi

	if [ ! `ls ${DATA_FOLDER}/*modified.fasta 2> /dev/null | wc -l ` -eq `ls ${DATA_FOLDER}/*modified.cds 2> /dev/null | wc -l ` ]; then
		log INFO "Preparing coding sequences for Ka/Ks analysis"
		source ${GFFREAD}/gff_profile
		gffread ${GENE_MODELS} -g ${REFERENCE} -x ${REFERENCE%.*}.cds
		ls ${DATA_FOLDER}/*modified.fasta | parallel "gffread ${GENE_MODELS} -g {} -x {.}.cds"
	fi

	log INFO "Extracting CDS information and conducting Ka/Ks analysis with KaKs calculator's YN and NG methods."
	ls ${DATA_FOLDER}/*modified.cds | parallel "python align.py {} ${REFERENCE%.*}.cds ${DATA_FOLDER} ${KAKSCALC} ${MUSCLE}" # ${TRIMAL}"
	log INFO "Ka/Ks analysis done. See the ${DATA_FOLDER} folder for the results"

fi
