#!/usr/bin/env python

from pylab import *
from collections import defaultdict
import sys

chroms = set([l.split()[0] for l in open(sys.argv[1], "rt")])

folder = sys.argv[2] 
x, y = [], []
allvars, variants = {}, {}
strains = [f for f in sys.argv[3:]]

for s in strains:
    allvars[s], variants = {}, {}
    for c in sorted(chroms):
        x, y = [], []
        with open(s, "rt") as f:
            for line in f:
                l = line.split()
                if line.startswith(c):
                    x.append(l[1])
                    y.append(l[3])
        variants[c] = (x,y)
    allvars[s] = variants

xh, yh = 3,2
f, axarr = subplots(xh, yh)

def draw_subplot(axarr, x,y, coord,max_y,title,color):	
    axarr[coord].set_title(title)
    axarr[coord].set_ylabel("# of variants")
    axarr[coord].set_ylim(0, int(max_y))
    axarr[coord].plot(x, y, color=color)
    return axarr

for c in chroms:
    f, axarr = subplots(xh, yh)
    f.suptitle("Variant density per strain for %s" % c, fontsize=14, fontweight="bold")

    max_y = max(max([allvars[s][c][1] for s in allvars]))
    colors = ["blue", "green", "red", "black", "brown", "purple"]
    coords = [(i,j) for i in range(xh) for j in range(yh)]


    for s, color, coord in zip(strains, colors, coords):
        title = s[s.index("kaks/")+5:s.index("_")]
        axarr = draw_subplot(axarr, allvars[s][c][0], allvars[s][c][1], coord, max_y, title, color)
    
    f.set_size_inches(24,12)
    savefig("%s/%s.png" % (folder, c), dpi=100)

