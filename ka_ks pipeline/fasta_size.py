import sys
from collections import defaultdict

fasta = sys.argv[1]
entries = defaultdict(str)

with open(fasta) as f:
    for l in f:
        if l.startswith(">"):
	    entry = l.rstrip()[1:]
        else:
            entries[entry] += l.rstrip()

for k, v in sorted(entries.items(), key=lambda(x,y): len(y)):
    print k + "\t" + str(len(v))
