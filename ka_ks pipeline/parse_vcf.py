import sys

infile = sys.argv[1]
sample = sys.argv[2]
index = 0

##Multi-sample references
#remove indels
#remove variant calls with equal probabilities
#remove homozygous reference calls (0/0)
with open(sys.argv[1]) as f:
	for line in f:
		if "INDEL" in line:
			continue
		if line.startswith("##"):
			print line.rstrip()
			continue
		if line.startswith("#CHROM"):
			for i,e in enumerate(line.split()[9:]):
				if e == sample:
					index = i+9
					break
			print "\t".join(line.split()[:9]) + "\t"+line.split()[index]
			continue
		l = line.split()
		probs = set(l[index].split(":")[1].split(","))
		if len(probs) > 1 and not l[index].split(":")[0] == "0/0":
			print "\t".join(line.split()[:9]) + "\t"+line.split()[index]

