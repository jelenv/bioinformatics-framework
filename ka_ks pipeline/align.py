import sys
import os
import cPickle as pickle

SEQ1		= sys.argv[1]
REFERENCE	= sys.argv[2]
DATA_FOLDER	= sys.argv[3]
KAKSCALC 	= sys.argv[4]
MUSCLE		= sys.argv[5]
#MACSE		= sys.argv[5]

if SEQ1 == REFERENCE:
    exit()

def remove_stop(fasta):
    new = ""
    stop_codons = set(["TAG", "TAA", "TGA"]) #only universal genetic code for now
    for i in range(0, len(fasta), 3):
        new += fasta[i:i+3] if fasta[i:i+3] not in stop_codons else "" 
    return new

def parse_fasta(fasta):
    entry, seq = "", ""
    with open(fasta, "rt") as f:
        genes = {}
        for l in f:
            if l.startswith(">") and not entry:
                entry = l.rstrip()
                continue
            elif l.startswith(">") and entry:
                genes[entry] = seq 
                entry = l.rstrip()
                seq = ""
                continue
            seq += l.rstrip()#remove_stop(l.rstrip()) # Remove internal in-frame stop codons
        genes[entry] = seq
    return genes

s1, s2 = parse_fasta(SEQ1), parse_fasta(REFERENCE)
seqname = SEQ1[SEQ1.index("kaks/")+5:]

#os.system("rm -f temp*")

## Align sequences pairwise with muscle and write a temporary axt-like alignment file 
## for the Ka/Ks analysis with Kaks calculator
#for i, gene in enumerate(s1.keys()):
#    with open("tempmuscle_%s" % seqname, "wt") as f2:
#        f2.writelines([gene+"\n", s1[gene]+"\n", gene+"\n", s2[gene]+"\n\n"])
#    os.system("%s -in tempmuscle_%s >> tempfastakakss_%s; echo >> tempfastakakss_%s" % (MUSCLE, seqname, seqname, seqname ))	

###USE TRIMAL
#os.system("cat tempfastakakss_%s | awk '{if(substr($0, 0, 1) == \">\"){c+=1} if(substr($0, 0, 1) == \">\" && c%2==1){print \"\n\"$0} else {print}}' > trimal_correct.fasta}'", (seqname))
#os.system("tail -n +2 trimal_correct.fasta > tempfastakakss_%s" % seqname)

###USE MACSE
#for i, gene in enumerate(s1.keys()):
#	with open("tempmacse_%s" % seqname, "wt") as f2:
#		f2.writelines([gene+"\n", s1[gene]+"\n", gene+"\n", s2[gene]+"\n"])
#	os.system("java -jar -Xmx4G %s -prog alignSequences -out_NT neki_%s -seq tempmacse_%s -out_AA /dev/null && cat neki_%s >> tempfastakakss_%s && echo >> tempfastakakss_%s" % (MACSE, seqname, seqname, seqname, seqname, seqname ))	

###SNPS ONLY (no alignment needed)
for i, gene in enumerate(s1.keys()):
    with open("tempfastakakss_%s" % seqname, "at") as f2:
        f2.writelines([gene+"\n", s1[gene]+"\n", gene+"\n", s2[gene]+"\n\n"])

entry = ""
length = 0
with open("tempfastakakss_%s" % seqname) as g:
    with open("%s/alignments_%s.axt" % (DATA_FOLDER, seqname), "wt") as out:
        length = 0
        for l in g:
            if l.startswith(">") and not entry:
                entry = l[1:]
                out.write(entry)
                continue
            elif l.startswith(">") and entry:
                out.write((3-(length % 3)) * "-")
                length = 0
                out.write("\n")
                continue
            elif len(l) == 1:
                entry = ""
                out.write((3-(length % 3)) * "-")
                length = 0
                out.write("\n\n")
                continue
            else:
                length += len(l.rstrip())
                out.write(l.rstrip())

os.system("%s -i %s/alignments_%s.axt -o %s/results_%s.kaks -m NG -m YN" % (KAKSCALC, DATA_FOLDER, seqname, DATA_FOLDER, SEQ1[SEQ1.index("/kaks/")+6:SEQ1.index(".cds")]))
os.system("rm -f temp*")

