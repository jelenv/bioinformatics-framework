# About


This is a repository of code developed during my stay at the Bioinformatics and Systems Biology group at [PSB-VIB](http://www.psb.ugent.be/bsb)

The folders represent different pipelines I implemented and the `__imports.sh` script houses all the includes and static variables used in other scripts.



| **Folder**		| Purpose																																|
|:-----------------:|:--------------------------------------------------------------------------------------------------------------------------------------|
| *Annotation*		| Use the maker pipeline to produce high quality annotations, by integrating different types of information								|
| *Assembly*		| Use the A5 or the Velvetoptimiser pipelines to do de-novo assembly of genomes from Illumina reads										|
| *CNV*				| Use any of the 4 different programs to conduct a CNV analysis on a BAM mapping file or a reference genome sequence					|
| *Ka/Ks pipeline*	| Get variant calls from BAM mapping files, modify the reference according to those variants, align them and conduct a Ka/Ks analysis	|
| *Phylogeny*		| Get variant calls from BAM mapping files, concatenate the variants and align them to be used in MEGA									|


# Installation

Before running the pipelines, make sure that all the necessary tools are installed on your system. You can view all the tools used in the `__imports.sh` script.
First you have to set the paths for binaries in `__imports.sh` according to your system setup and the `$DATA` variable to point to the directory where you want to store results.

Once you set those, run the `__imports.sh` script to create the necessary folders.


# Parallelization

The framework assumes you have the GNU programs *nproc* and *parallel* installed. 

By using them, all the programs that can be parallelized will always use all of the available CPUs by default.


# Usage

Each pipeline starts by setting up the input files in the **appropriate** data folders. In order to use proper input, you need to modify the heads of the main pipeline scripts to point to them.
There are also other settings in the script heads sometimes that can be modified, or left at default (like snp filtering parameters).
Also, the pipelines assume certain names for your input files, which are documented in the paragraphs below. Once the input files are in place and the scripts are properly modified,
you can run the desired pipeline. 


### Annotation

*Not yet implemented*


### Assembly

*changes to follow, not fully documented*

- Pipeline assumes different samples, so set the sample name in the head of `assembly.sh`  _This will be changed in future versions_
- Rename your paired-end reads in the *sample* data folder to end with _1.fastq.gz_ and _2.fastq.gz_ respectively. 
- Set the assembly program used in the script head `$A5` _or_ `$VELOPT` 


### CNV

*changes to follow, not fully documented*

- Different programs used in this pipeline require either _.bam_ or _.fasta_ files as input. Set these in the `cnv.sh` script head accordingly.
- CNV-Seq works by comparing different _.bam_ files to find CNVs. Set the `REF` and `TEST` variables in the `cnv.sh` script head if you want to use that program.
- Set the program you want to use with the `PROGRAM` variable in `cnv.sh`. You can choose between `$CNVNATOR, $PINDEL, $GASV or $CNVSEQ`


### Ka/Ks pipeline

- Rename all your _.bam_ files in the data folder to end with _mapping.bam_.
- Set the other inputs, snp filtering parameters and window size for the snp-distribution analysis in the `kaks.sh` script head.


### Phylogeny

*changes to follow, not fully documented*

- Preferably, use a multi-pileup _.bcf_ file in the data folder 
- Set the snp filtering parameters in the `phylogeny.sh` script head
- Use the final _.fasta_ file in a phylogeny program (e.g. MEGA) or the _.nex_ file in a treeviewing program


# Example

Since an example is more informative than lengthy documentation, an example for the Ka/Ks pipeline is presented below:

- Download the framework from the bitbucket repository `git clone git@bitbucket.org:Flashpoint/vib.git`
- Set the binaries paths and the path to your data folder in `__imports.sh` and run the script to create the data folder and its subfolders
- copy all the _.bam_ files to `$DATA/kaks`, along with a _.fasta_ genome (e.g. genome.fasta), _.gff3_ gene annotations (e.g. genes.gff3) and _.gff3_ repeat annotations (e.g. repeats.gff3)
- modify the start of `ka_ks pipeline/kaks.sh` to point the variables to your files.


`REFERENCE=${DATA_FOLDER}/genome.fasta`


`GENE_MODELS=${DATA_FOLDER}/genes.gff3`


`REPEATS=${DATA_FOLDER}/repeats.gff3`


- modify snp calling parameters by deleting a lower bound on snp calling `SNPFILTERS="-D 1000000 -Q 20"` and leave rest as default
- save your changes and run the `kaks.sh` script




