#!/usr/bin/env bash

source ../__imports.sh

### Paths to data
DATA_FOLDER=$CNV_DATA
MAP=$DATA_FOLDER/1953_sorted.bam
REFERENCE=$DATA_FOLDER/vert2.fasta
FASTAS=$DATA_FOLDER/parsed_fastas
NAME=`basename ${MAP}`

REF=$DATA_FOLDER/tab2_sorted.bam
TEST=$DATA_FOLDER/1953_sorted.bam
PROGRAM=$PINDEL

#########################################################################################
# PART 1: Use different programs to conduct the CNV analysis							#
#########################################################################################
#																						#
# The programs differ by the type of analysis they use to calculate the CNV regions		#
# and by the inputs they need. There are 4 types of approaches to this calculation:		#
#																						#
# --- Paired-end mapping (PEM)															#
# --- Split-read (SR)																	#
# --- de-novo assembly (AS)																#
# --- Read-depth (RD)																	#
#																						#
# CNVnator ==>> uses read mapping info from BAM files and separate .fa files for		#
#				each chromosome/scaffold in the reference .fasta file. (RD)				#
#																						#
# PINDEL   ==>>	uses read mapping info from BAM files and a faidx indexed reference		#
#				(SR)																	#
#																						#
# GASV	   ==>> uses read mapping info from BAM files (PEM)								#
#																						#
# CNV-seq  ==>>	uses coverage info from BAM files and a genome size estimate (RD)		#
#				works similarly like aCGH.												#
#																						#
#########################################################################################


if [ $PROGRAM == $CNVNATOR ]; then
	ls ${DATA_FOLDER}/CNVnator/*.vcf 2>&1 > /dev/null
	if [ ! $? -eq 0 ]; then
		log INFO "Preparing fasta sequences from ${REFERENCE} for CNVnator"
		if [ ! -d $FASTAS ]; then
			mkdir -p ${FASTAS}
			mkdir -p ${DATA_FOLDER}/CNVnator
		fi
		python split_fasta.py ${REFERENCE} 
		source ${ROOT}

		for f in `ls *.fa`; do
			log INFO "Started work on chromosome ${f%.*}"
			${CNVNATOR} -root rhizome.root -genome ${REFERENCE} -chrom ${f%.*} -unique -tree ${MAP}
			${CNVNATOR} -root rhizome.root -genome ${REFERENCE} -chrom ${f%.*} -his 100 
			${CNVNATOR} -root rhizome.root -genome ${REFERENCE} -chrom ${f%.*} -stat 100 
			${CNVNATOR} -root rhizome.root -genome ${REFERENCE} -chrom ${f%.*} -partition 100 
			${CNVNATOR} -root rhizome.root -genome ${REFERENCE} -chrom ${f%.*} -call 100 > cnv_analysis_${NAME%.*}_${f%.*}.txt
			perl ${CNV2VCF} cnv_analysis_${NAME%.*}_${f%.*}.txt > cnv_analysis_${NAME%.*}_${f%.*}.vcf 2> /dev/null
		done
		mv *.fa ${FASTAS}
		mv *.txt *.vcf ${DATA_FOLDER}/CNVnator
		log INFO "CNV analysis complete. Check the ${DATA_FOLDER} folder for results."
	else
		log DONE "CNV analysis with CNVnator already done."
	fi
elif [ $PROGRAM == $PINDEL ]; then
	rm -f pindel_config.txt
	for f in `ls ${DATA_FOLDER}/*sorted*.bam`; do
		printf "${f}\t500\t${f%.*}\n" >> pindel_config.txt
	done
	if [ ! -e ${REFERENCE}.fai ]; then
		${SAMTOOLS} faidx ${REFERENCE}
	fi
	${PINDEL}/pindel -f ${REFERENCE} -T ${NPROC} -i pindel_config.txt -c ALL -o ${DATA_FOLDER}/pindel_output
	${PINDEL}/pindel2vcf -r ${REFERENCE} -R reference -d `date` -p ${DATA_FOLDER}/pindel_output
elif [ $PROGRAM == $GASV ]; then
	sed -e "s|BAMFILE=|BAMFILE=$MAP|g" -e "s|GASVDIR=|GASVDIR=${GASVDIR}|g" $GASV > run_gasv.sh ##kudos to the last post in http://askubuntu.com/questions/76808/how-to-use-variables-in-sed 
	bash run_gasv.sh
	rm run_gasv.sh
elif [ $PROGRAM == $CNVSEQ ]; then
	
	#http://tiger.dbs.nus.edu.sg/cnv-seq/doc/manual.pdf
	samtools view -F 4 $REF | perl -lane 'print "$F[2]\t$F[3]"' > ${DATA_FOLDER}/ref.hits
	samtools view -F 4 $TEST | perl -lane 'print "$F[2]\t$F[3]"' > ${DATA_FOLDER}/test.hits
	perl ${CNVSEQ} --test ${DATA_FOLDER}/test.hits --ref ${DATA_FOLDER}/ref.hits --genome-size 33000000
	printf "library(cnv)\ndata <- read.delim('test.hits-vs-ref.hits.log2-0.6.pvalue-0.001.minw-4.cnv')\nplot.cnv(data)" > ${DATA_FOLDER}/cnv_graph.R
	mv test.hits-vs-ref* ${DATA_FOLDER}
	log INFO "R script for further CNV data interrogation prepared in ${DATA_FOLDER}/cnv_graph.R"
	#Rscript cnv_graph.R
fi

