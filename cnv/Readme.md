#Copy-number Variation (CNV)
----------------------------

The pipeline can use different inputs (genome (.fasta), mapping (.bam), reads (.fastq)), depending on the underlying program used, to predict CNVs in the genome.
Programs used here are:


- CNVnator
- PINDEL
- GASV
- CNV-Seq
