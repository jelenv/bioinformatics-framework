#!/usr/bin/env bash

####################
# IMPORTANT NOTICE #
#####################################################################################
# This file stores all the paths necessary to run the pipelines (binaries, data)	#
# Change the binaries paths that differ on your system setup from the default ones	#
#####################################################################################	

### Paths to binaries
SAMTOOLS="/usr/bin/samtools" 
BCFTOOLS="/usr/bin/bcftools"
VCFUTILS="/usr/share/samtools/vcfutils.pl"
BEDTOOLS="/usr/bin/bedtools"
CODEML="/usr/bin/codeml"
KAKSCALC="/home/vid/KaKs_Calculator1.2/src/KaKs_Calculator"
SNPEFF="/home/vid/SnpEff"
GFFPROFILE="/home/vid/gffextractor/gff_scripts/gff_profile"
GFFREAD="/usr/bin/gffread"
MUSCLE="/usr/bin/muscle"
MACSE="/home/vid/macse/macse_v1.01b.jar"
PRANK="/home/vid/prank-msa/src/prank"
GUIDANCE="/home/vid/guidance.v1.41/www/Guidance/guidance.pl"
TRIMAL="/home/vid/trimAl/source/trimal"
A5="/home/vid/a5_miseq_linux_20140401/bin/a5_pipeline.pl"
VELOPT="/home/vid/VelvetOptimiser-2.2.5/VelvetOptimiser.pl"
CNVNATOR="/home/vid/cnvnator/CNVnator_v0.3/src/cnvnator"
CNV2VCF="/home/vid/cnvnator/CNVnator_v0.3/cnvnator2VCF.pl"
PINDEL="/home/vid/toolsforcnv/pindel"
GASV="/home/vid/toolsforcnv/gasv/bin/GASVPro-HQ.sh"
CNVSEQ="/home/vid/toolsforcnv/cnv-seq/cnv-seq.pl"
ROOT="/home/vid/root/bin/thisroot.sh" 
GASVDIR="/home/vid/toolsforcnv/gasv"
NPROC="/usr/bin/nproc"
PARALLEL="/usr/bin/parallel"
TRANSDECODER="/home/vid/TransDecoder_r20140704"
GMSN="/home/vid/genemarkS/gmsuite/gmsn.pl"
MAKER="/home/vid/maker/bin/maker"
TRNASCAN="/home/vid/tRNAscan-SE-1.23/"

### Path to the root data folder
DATA="../../data" #preferably use a full path instead of a relative path 

ANNOTATION_DATA=$DATA/annotation
ASSEMBLY_DATA=$DATA/assembly
CNV_DATA=$DATA/cnv
KAKS_DATA=$DATA/kaks
PHYLOGENY_DATA=$DATA/phylogeny

if [ ! -d $ANNOTATION_DATA ] || [ ! -d $ASSEMBLY_DATA ] || [ ! -d $CNV_DATA ] || [ ! -d $KAKS_DATA ] || [ ! -d $PHYLOGENY_DATA ]; then
    mkdir -p {$ANNOTATION_DATA,$ASSEMBLY_DATA,$CNV_DATA,$KAKS_DATA,$PHYLOGENY_DATA}
fi

log() {
    local level=${1?}
    shift
    local code= line="[$(date '+%F %T')] $level: $*"
    if [ -t 2 ]
    then
        case "$level" in
        INFO) code=36 ;;
        DEBUG) code=30 ;;
        WARN) code=33 ;;
		DONE) code=32 ;;
        ERROR) code=31 ;;
        *) code=37 ;;
        esac
        echo -e "\e[${code}m${line}\e[0m"
    else
        echo "$line"
    fi >&2
}
